<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('auth.register');
    }

    public function store(Request $request)
    {
        dd($request);
        return redirect('welcome');
    }

    public function welcome()
    {
        return view('auth.welcome');
    }
}
