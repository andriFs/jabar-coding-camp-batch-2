<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>

<body>
    <h1>Berlatih Function PHP</h1>
    <?php

echo "<h3> Soal No 1 Greetings </h3>";
/* 
Soal No 1
Greetings
Buatlah sebuah function greetings() yang menerima satu parameter berupa string. 

contoh: greetings("abduh");
Output: "Halo Abduh, Selamat Datang di Jabar Coding Camp!"
*/

// Code function di sini
function greetings($nama){
    echo "Halo $nama, Selamat Datang di Jabar Coding Camp! <br>";
}


// Hapus komentar untuk menjalankan code!
greetings("Bagas");
greetings("Wahyu");
greetings("Abdul"); 
greetings("Abduh");

echo "<br>";

echo "<h3>Soal No 2 Reverse String</h3>";
/* 
Soal No 2
Reverse String
Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya menggunakan function dan looping (for/while/do while).
Function reverseString menerima satu parameter berupa string.
NB: DILARANG menggunakan built-in function PHP sepert strrev(), HANYA gunakan LOOPING!

reverseString("abdul");
Output: ludba

*/

// Code function di sini 
function reverse($nama1){
    $panjangKata = strlen($nama1);
    $tampung = "";
    for($a = ($panjangKata-1); $a >= 0; $a--){
        $tampung .= $nama1[$a];
    }
    return $tampung;
}

function reverseString($nama2){
    $output = reverse($nama2);
    echo $output . "<br>";
}

// Hapus komentar di bawah ini untuk jalankan Code
reverseString("abdul");
reverseString("abduh");
reverseString("Bootcamp");
reverseString("We Are JCC Developers");
echo "<br>";

echo "<h3>Soal No 3 Palindrome </h3>";
/* 
Soal No 3 
Palindrome
Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan. 
Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.
NB: 
Contoh: 
palindrome("katak") => output : "true"
palindrome("jambu") => output : "false"
NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari jawaban no.2!

*/


// Code function di sini

function palindrome($kata){
    $cekBalikanKata = reverse($kata);
    if($kata === $cekBalikanKata){
        echo "palindrome(".$kata.") => output : True <br>";
    }else{
        echo "palindrome(".$kata.") => output : False <br>";
    }
}

// Hapus komentar di bawah ini untuk jalankan code
palindrome("civic") ; // true
palindrome("nababan") ; // true
palindrome("jambaban"); // false
palindrome("racecar"); // true


echo "<h3>Soal No 4 Tentukan Nilai </h3>";
/*
Soal 4
buatlah sebuah function bernama tentukan_nilai . Di dalam function tentukan_nilai yang menerima parameter 
berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” 
Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar 
sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
*/

// Code function di sini
function tentukan_nilai($nilai){
    if($nilai >= 85 && $nilai <= 100){
        return "Sangat Baik <br>";
    }else if($nilai >= 70 && $nilai < 85){
        return  "Baik <br>";
    }else if($nilai >= 60 && $nilai < 70){
        return "Cukup <br>";
    }else{
        return "Kurang <br>";
    }
}

// Hapus komentar di bawah ini untuk jalankan code
echo "Nilai 98 : " . tentukan_nilai(98); //Sangat Baik
echo "Nilai 76 : " . tentukan_nilai(76); //Baik
echo "Nilai 67 : " . tentukan_nilai(67); //Cukup
echo "Nilai 43 : " . tentukan_nilai(43); //Kurang

echo "<h3>Soal No 5 Quis </h3>";

function xo($str) {
    //Code disini
    $data = 'oxooxo oxx';
    $string = substr_count($data, $str);
    if($string == 0 ){
        return "Benar <br>";
    }else if ($string == 1){
        return "salah <br>";
    }
    
}
    // Test Cases
echo "output xo = " . xo('xoxoxo'); // "Benar"
echo "output xo = " . xo('oxooxo'); // "Salah"
echo "output xo = " . xo('oxx'); // "Salah"
echo "output xo = " . xo('xxooox'); // "Benar"
echo "output xo = " . xo('xoxooxxo'); // "Benar"


echo "<h3>Soal No 6 Quis </h3>";

function pagar_bintang($integer){
    for($j = 1; $j <= $integer; $j++){
        for($k = $j; $k >= 1; $k--){
            echo " * " ;
        }
        echo "<br>";
    }
      
}
    
    echo pagar_bintang(5);
    echo pagar_bintang(8);
    echo pagar_bintang(10);
?>

</body>

</html>